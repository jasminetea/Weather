//
//  DataModel.swift
//  Weather
//
//  Created by Malea Kotelo on 2017/07/21.
//  Copyright © 2017 dvt. All rights reserved.
//

import UIKit

class DataModel {
    
    //Update images as the weather conditions change
    
    func updateWeatherImages(_ id: Int, index: Int, callback:(_ index: Int, _ name: String)->()) {
        
        switch (id) {
            
        case 200...232 :
            callback(index, "nt_tstorms")
            
        case 300...321 :
            callback(index, "OccLightRain")
            
        case 500...531 :
            callback(index, "nt_rain")
            
        case 600...622 :
            callback(index, "nt_snow")
            
        case 601...700 :
            callback(index, "Snowflake")
            
        case 701 :
            callback(index, "Mist")
            
        case 711...771 :
            callback(index, "hazy")
            
        case 781 :
            callback(index, "Tornado")
            
        case 800 :
            callback(index, "clear")
            
        case 801...804  :
            callback(index, "partlycloudy")
            
        default :
            callback(index, "clear")
        }
        
    }

    
}
