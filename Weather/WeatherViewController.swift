//
//  WeatherViewController.swift
//  Weather
//
//  Created by Malea Kotelo on 2017/07/19.
//  Copyright © 2017 dvt. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class WeatherViewController: UIViewController,CLLocationManagerDelegate, NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var conditionLabel: UIImageView!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!

    
    public enum Status {
        case success
        case failure
    }
    
    open class Response {
        open var status: Status = .failure
        open var object: JSON? = nil
        open var error: NSError? = nil
    }
    
    //Open Weather URL ID/s
    let URL = "http://api.openweathermap.org/data/2.5/forecast"
    let ID = "614e860eb864062f9082aec54bdf35de"
    
    var startLocation: CLLocation!
    let locationManager = CLLocationManager()
    let dataModel = DataModel()
    let size = CGSize(width: 30, height: 30)
    

    override func viewDidLoad() {
        super.viewDidLoad()
    
        determineCurrentLocation()
        
        //Animation to indicate data is being loaded
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
    }

    
    func determineCurrentLocation() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            
            locationManager.stopUpdatingLocation()
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
         
            updateWeatherInfo(latitude: latitude, longitude: longitude)
            
        }
        
    }
    
    func updateWeatherInfo(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        retrieveForecast(latitude, longitude: longitude,
                         success: {
                            response in print(response)
                            //update the View if Weather information is received
                            self.updateUIView(json: response.object!)
                            //stop animation loader from showing
                            self.stopAnimating()
                        }, failure: {
                            response in print(response)
                            print("Error: " + response.error!.localizedDescription)
                            self.startAnimating(self.size, message: "Error: " + response.error!.localizedDescription, type: NVActivityIndicatorType(rawValue: 1)!)
        })
        
    }
    
    func retrieveForecast(_ latitude: CLLocationDegrees, longitude: CLLocationDegrees, success:@escaping (_ response: Response)->(), failure: @escaping (_ response:Response)->()) {
        
        let params = ["lat":latitude, "lon":longitude, "APPID":ID] as [String : Any]
        
        Alamofire.request(URL, parameters: params, encoding: URLEncoding.default)
            .responseJSON { response in
                if let JSONN = response.result.value {
                    
                    print("JSON: \(JSONN)")
                    print("Success: \(self.URL)")
                    let json = JSON(JSONN)
                    let response = Response()
                    response.status = .success
                    response.object = json
                    success(response)
                    
                } else {
                    print("no internet connection")
                    self.stopAnimating()
                    self.startAnimating(self.size, message: "Error: " + response.error!.localizedDescription, type: NVActivityIndicatorType(rawValue: 1)!)
                    
                }
        }
        
    }

    
    func updateUIView(json: JSON) {

        //Set values to labels
        
        //Convert country code to country name in order to set to the curent city label
        let main = json["list"][0]["main"]
        let englishLocale : Locale = Locale.init(identifier :  "en_US")
        let countryCode = json["city"]["country"].stringValue
        let country = (englishLocale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        let countryName = String(describing: country  != nil ? country  : "")
        let currentCity = json["city"]["name"].stringValue
        cityName.text = currentCity
        
        //set current Date and Time
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let todaysDate = dateFormatter.string(from: date)
        dateLabel.text = todaysDate

        // Set the Current Condition Description
        let weather = json["list"][0]["weather"][0]
        let currentWeatherCondition = weather["main"].stringValue
        weatherDescription.text = currentWeatherCondition
        
        let condition = weather["id"].intValue
        dataModel.updateWeatherImages(condition, index: 0, callback: self.updatePictures)
        
        //set the min and max labels
        let tempMax = main["temp_max"].double != nil ? main["temp_max"].double: 0.0
        let tempMin = main["temp_min"].double != nil ? main["temp_min"].double : 0.0
        let maximumTemp = convertTemperature(country: countryName, temperature: tempMax!)
        let minimumTemp = convertTemperature(country: countryName, temperature: tempMin!)
        minLabel.text = maximumTemp + " / "+minimumTemp
        
    }
    
    func convertTemperature(country: String, temperature: Double)->String {
        
        if (country == "US") {
            
            // Convert temperature to Fahrenheit if user is within the US
            let degreeFahrenheit = Int(round(((temperature - 273.15) * 1.8) + 32))
            return "\(degreeFahrenheit)°"
        } else {
            
            // Otherwise, convert temperature to Celsius
            let celsius = Int(round(temperature - 273.15))
            return "\(celsius)°"

        }
    }
    
    func updatePictures(index: Int, name: String) {
        conditionLabel.image = UIImage(named: name)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("Error \(error)")
    }
   
  

}


